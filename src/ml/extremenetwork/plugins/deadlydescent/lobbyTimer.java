package ml.extremenetwork.plugins.deadlydescent;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import me.confuser.barapi.BarAPI;

public class lobbyTimer  {
	private DeadlyDescent dd = DeadlyDescent.plugin;
	private int countdownTimer;
	private static boolean started;
	private float percent;

	public void start(final int time)
	{
		Bukkit.getScheduler().cancelTask(this.countdownTimer);
		if (started == true) {
			dd.$(0, "Stopping duplicate timer.");
			Bukkit.getScheduler().cancelTask(this.countdownTimer);
			return;
		}
		started = true;
		this.countdownTimer = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(dd, new Runnable()
		{
			int i = time;

			public void run()
			{
				for(Player p : Bukkit.getOnlinePlayers()){
					if(p.getWorld().getName().equals(dd.getLobbyName())){
						if (i <= 10) {
							p.sendMessage("Starting in " + i + " seconds!");
						}
						try {
							percent = (100 / dd.getLobbyWaitTime()) * i;
						} catch (ArithmeticException e) {
							throw new ArithmeticException("Failed to calculate (100/"+dd.getLobbyWaitTime()+")*"+i);
						}
						BarAPI.setMessage(p, "Starting in " + i, (float) percent);
						if (i == 1) {
							BarAPI.setMessage(p, "Please wait...", (float) 0);
						}
					}
				}
				this.i--; //decrement
				if (this.i <= 0)
				{
					cancel();
					finished();
					//ended
				}
			}
		}
		, 0L, 20L);
	}
	public void cancel()
	{
		Bukkit.getScheduler().cancelTask(this.countdownTimer);
		for(Player p : Bukkit.getOnlinePlayers()){
			if(p.getWorld().getName().equals(dd.getLobbyName())){
				BarAPI.removeBar(p);
			}
		}
	}
	public void finished() {
		readyGame readyGame = new readyGame();
		readyGame.warpToGame();
	}
}

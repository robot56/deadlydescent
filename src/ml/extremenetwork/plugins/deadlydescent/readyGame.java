package ml.extremenetwork.plugins.deadlydescent;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import me.confuser.barapi.BarAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;

public class readyGame {
	private DeadlyDescent dd = DeadlyDescent.plugin;
	private String[] worlds = dd.getGameWorldsArray();
	private List worldList = dd.getGameWorldsList();
	private game game = new game();

	public void collectLobbies() {
		for (String s: worlds) {
			dd.$(1, "Worlds to be loaded (or generated): " + s);
			//System.out.println(Prefix + "Worlds to be loaded (or generated): " + s);
			World world = Bukkit.getServer().createWorld(new WorldCreator(s));
		}
	}
	public void warpToGame() {
		World world = Bukkit.getServer().getWorld(getNextMap());
    	Location loc = new Location(world, world.getSpawnLocation().getX(), world.getSpawnLocation().getY(), world.getSpawnLocation().getZ());
		for(Player p : Bukkit.getOnlinePlayers()){
    	    if(p.getWorld().getName().equals(dd.getLobbyName())){
    	    	p.setFlying(false);
    	    	p.teleport(loc);
    	    }
    	}
        if (!game.startGame(world)) {
        	dd.$(4, "Failed to start game!");
        	for (Player p : Bukkit.getServer().getOnlinePlayers()) {
        		p.teleport(dd.getLobbyLocation());
        		p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Deadly Descent" + ChatColor.RESET + ChatColor.RED + " � " + ChatColor.RESET + "Sorry, there seems to have been an error starting the game :(.");
        	}
        }
	}
	
	public String getNextMap() {
		if (dd.getConfig().getBoolean("game.randomGame")) {
			Random rand = new Random();
			dd.$(0, "# of worlds in Array:" + worlds.length);
			int max = worlds.length - 1;
			int min = 0;
			int randomNum = rand.nextInt((max - min) + 1) + min;
			dd.$(0, "Random # picked: " + randomNum);
			dd.$(0, "# of worlds in List: " + worldList.size());
			String nextWorld = worldList.get(randomNum).toString();
			dd.$(0, "Next world should be: " + nextWorld);
			return nextWorld;
		} else {
			return "icelands";
		}
	}
}

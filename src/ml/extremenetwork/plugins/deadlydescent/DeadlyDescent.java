package ml.extremenetwork.plugins.deadlydescent;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class DeadlyDescent extends JavaPlugin implements Listener {
	public static Logger log = Logger.getLogger("Minecraft");
	private PluginDescriptionFile pdf = this.getDescription();
	public String Prefix = "[" + pdf.getName() + "]";
	public final lobby lobby = new lobby();
	public static DeadlyDescent plugin;

	public boolean gameInProgress;
	private int lobbySpawnX;
	private int lobbySpawnY;
	private int lobbySpawnZ;
	private int maxPlayers;
	private int minPlayers;
	private int lobbyWaitTime;
	private boolean lobbyProtection;
	private String lobbyWorldName;
	private Location lobbySpawn;

	@Override
	public void onDisable() {
		plugin = null;
	}
	@Override
	public void onEnable() {
		this.plugin = this;
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(this.lobby, this);
		loadConfiguration();
		readyGame readyGame = new readyGame();
		readyGame.collectLobbies();
		$(1, "Deadly Descent v" + pdf.getVersion() + " has been enabled!");
	}

	public void loadConfiguration() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		lobbyWorldName = getConfig().getString("lobby.worldName");

		if (getConfig().getString("lobby.location") == null || getConfig().getString("lobby.location").isEmpty()) {
			lobby lobby = new lobby();
			lobbySpawn = lobby.getLobbySpawn(lobbyWorldName);
			int lobbySpawnX = lobbySpawn.getBlockX();
			int lobbySpawnY = lobbySpawn.getBlockY();
			int lobbySpawnZ = lobbySpawn.getBlockZ();
			getConfig().set("lobby.location", lobbySpawnX + " " +  lobbySpawnY + " " + lobbySpawnZ);
			saveConfig();
			$(0, "Spawn location for lobby '" + lobbyWorldName + "' appears to be: " + lobbySpawn.toString());
		} else {
			try {
				String[] lobbyLocation = getConfig().getString("lobby.location").split(" ");
				World lobby = Bukkit.getServer().getWorld(lobbyWorldName);
				lobbySpawn = new Location(lobby, Integer.parseInt(lobbyLocation[0]), Integer.parseInt(lobbyLocation[1]), Integer.parseInt(lobbyLocation[2]));
				$(0, "Spawn location for lobby '" + lobbyWorldName + "' appears to be: " + lobbySpawn.toString());
			} catch (Exception e) {
				throw new IllegalArgumentException("Invalid lobby location ('" + getConfig().getString("lobby.location") + "') specified!", e);
			}
		}
		try {
			maxPlayers = getConfig().getInt("game.maxPlayers");
			minPlayers = getConfig().getInt("game.minPlayers");
		} catch (NumberFormatException e) {
			log.severe(Prefix + "You have specified an invalid player amount, check your config!");
			throw new NumberFormatException();
		}
		if (maxPlayers > 25 || maxPlayers < 2) {
			log.warning(Prefix + "The specified player amount is either too large, or too little, defaulting to 15.");
			maxPlayers = 15;
		}
		if (minPlayers > maxPlayers || minPlayers < 2) {
			log.warning(Prefix + "The specified minPlayers amount is either greater than the maxPlayers, or it is too little, defaulting to 8.");
			minPlayers = 8;
		}
		try {
			lobbyWaitTime = getConfig().getInt("lobby.waitTime");
		} catch (NumberFormatException e) {
			log.severe(Prefix + "The wating time specified is not valid, check your config!");
			throw new NumberFormatException();
		}
		if (lobbyWaitTime < 2 || lobbyWaitTime > 200) {
			log.warning(Prefix + "The specified waiting time is either too little or too large (>200s), defaulting to 30.");
			lobbyWaitTime = 30;
		}
		lobbyProtection = getConfig().getBoolean("lobby.protect");
		$(0, "Config 'plugin.prefix' = " + getConfig().getString("plugin.prefix"));
		$(0, "Config 'plugin.credits' = " + getConfig().getString("plugin.credits"));
		$(0, "Config 'plugin.debug' = " + getConfig().getString("plugin.debug"));
		$(0, "Config 'lobby.worldName' = " + getConfig().getString("lobby.worldName"));
		$(0, "Config 'lobby.location' = " + getConfig().getString("lobby.location"));
		$(0, "Config 'lobby.waitTime' = " + getConfig().getString("lobby.waitTime"));
		$(0, "Config 'lobby.fly' = " + getConfig().getString("lobby.fly"));
		$(0, "Config 'lobby.protect' = " + getConfig().getString("lobby.protect"));
		$(0, "Config 'lobby.scoreboardTitle' = " + getConfig().getString("lobby.scoreboardTitle"));
		$(0, "Config 'game.safe' = " + getConfig().getString("game.safe"));
		$(0, "Config 'game.safeTime' = " + getConfig().getString("game.safeTime"));
		$(0, "Config 'game.maxPlayers' = " + getConfig().getString("game.maxPlayers"));
		$(0, "Config 'game.minPlayers' = " + getConfig().getString("game.minPlayers"));
		$(0, getGameWorldsArray().toString());
	}
	
	public void $(int level, String message) {
		switch (level) {
			case 0:
				if (getConfig().getBoolean("plugin.debug")) {
					log.info(Prefix + " [DEBUG] " + message);
				}
				break;
			case 1:
				log.info(Prefix + " " + message);
				break;
			case 2:
				log.warning(Prefix + " " + message);
			case 3:
				log.fine(Prefix + " " + message);
			case 4:
				log.finer(Prefix + " " + message);
			case 5:
				log.finest(Prefix + " " + message);
			case 6:
				log.severe(Prefix + " " + message);
		}
	}

	public void setInProgress(Boolean bool) {
		gameInProgress = bool;
		if (bool) {
			game game = new game();
			getServer().getPluginManager().registerEvents(game, this);
		}
	}
	
	/*public void disablePlugin() {
		// Called when an exception happens that cannot be recovered from.
		// Set whitelist to prevent server damage
		Bukkit.getServer().setWhitelist(true);
		// Kick all players
		for(Player player : Bukkit.getServer().getOnlinePlayers()) {
		    player.kickPlayer("Internal Server Error");
		}
		Bukkit.getServer().getPluginManager().disablePlugin(this);
	}*/
	
	public String[] getGameWorldsArray() {
		List<?> gamesLists = getConfig().getList("games.list");
		System.out.println("Fetched games list.");
		String[] gamesList = gamesLists.toArray(new String[gamesLists.size()]);
		System.out.println("Converted to array.. OK.");
		return gamesList;
	}
	public List getGameWorldsList() {
		List<?> gamesList = getConfig().getList("games.list");
		return gamesList;
	}
	public FileConfiguration getConfiguration() {
		return getConfig();
	}
	public int getGameTime() {
		int time = getConfig().getInt("game.time");
		if (time < 5 || time > 3600) {
			log.warning(Prefix + "Game time limit is out of bounds, automatically setting to 900 seconds.");
			time = 900;
		}
		return time;
	}
	public int getYToWin(String world) throws InvalidConfigurationException {
		int var = getConfig().getInt("games." + world + ".y");
		if (var < 1) {
			$(6, "You specified an invalid y-value for world \""+world+"\"!");
			throw new org.bukkit.configuration.InvalidConfigurationException();
		}
		return var;
	}
	public int getFoodTime() {
		int var = getConfig().getInt("game.foodInterval");
		if (var < 2 || var > 3600) {
			log.warning(Prefix + "Game food interval limit is out of bounds, automatically setting to 900 seconds.");
			var = 60;
		}
		return var;
	}
	public boolean isCredits() {
		return getConfig().getBoolean("plugin.credits");
	}
	int[] getLobbySpawn() {
		int[] s = {lobbySpawnX, lobbySpawnY, lobbySpawnZ};
		return s;
	}
	Location getLobbyLocation() {
		return lobbySpawn;
	}
	String getLobbyName() {
		return lobbyWorldName;
	}
	World getLobbyWorld() {
		return Bukkit.getServer().getWorld(lobbyWorldName);
	}
	Boolean isLobbyProtected() {
		return lobbyProtection;
	}
	int getLobbyWaitTime() {
		return lobbyWaitTime;
	}
	int getMaxPlayers() {
		return maxPlayers;
	}
	int getMinPlayers() {
		return minPlayers;
	}
}


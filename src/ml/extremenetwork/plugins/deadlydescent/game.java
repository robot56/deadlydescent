package ml.extremenetwork.plugins.deadlydescent;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.*;

import me.confuser.barapi.BarAPI;

public class game implements Listener {
	private DeadlyDescent dd = DeadlyDescent.plugin;
	private final int gameTime = dd.getGameTime();
	private final int lobbyTPTime = 5;
	private int yToWin;
	// Internal-lock that can only be set in this class
	private static Boolean game = false;
	private static World world;
	private static Objective objective;
	private static double minutes_static;
	private int gameTick;
	private int foodTick;
	private int lobbyTP;
	private float percent;

	public boolean startGame(World world) {
		if (dd.gameInProgress) {
			throw new IllegalStateException("Game is already in progress!");
		}
		dd.setInProgress(true);
		dd.$(1, "Game has started!");
		game = true;
		this.world = world;
		try {
			yToWin = dd.getYToWin(world.getName());
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
			return false;
		}
		newGame();
		dd.$(0, "Game is currently set to: " + game);
		scoreboard();
		return true;
	}

	private void newGame() {
		dd.$(0, "Game time is: " + gameTime);
		long foodTime;
		try {
			foodTime = (long)(dd.getFoodTime() * 20);
		} catch (ArithmeticException e) {
			throw new ArithmeticException("Invalid food time: " + dd.getFoodTime());
		}
		this.gameTick = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(dd, new Runnable() {
			int i = gameTime;
			public void run() {
				//dd.$(0, "Lobby ticking OK!");
				try {
					percent = ((float)100 / (float) i) * i;
				} catch (ArithmeticException e) {
					throw new ArithmeticException("Failed to calculate (100/" + i + ")*" + i);
				}
				for(Player p : Bukkit.getOnlinePlayers()){
					if(p.getWorld().getName().equals(world.getName())){
						if (i <= 10) {
							p.sendMessage(ChatColor.RED + "Game ends in " + i + " seconds!");
						}

						if (percent % 2 != 0) {
							// get rid of bars at odd number; fix major client lag				
							BarAPI.removeBar(p);
							//dd.$(0, "Removed boss bar to clear lagg.");
						}
						BarAPI.setMessage(p, ChatColor.RED + "Game ends in " + i + " seconds!", (float) percent);
						// client lagg sucks :(
						/*if (i < 200) {
							BarAPI.setMessage(p, ChatColor.RED + "Game ends in " + i + " seconds!", (float) percent);
						} else {
							double minutes = Math.round((i % 3600) / 60);
							if (BarAPI.hasBar(p) && minutes == minutes_static) {} else {
								minutes_static = minutes;
								dd.$(0, "" + minutes_static + " " + i);
								BarAPI.setMessage(p, ChatColor.GREEN + "Game ends in " + minutes + " minutes.", (float) percent);
							}
						}*/
						if (i == 0) {
							BarAPI.setMessage(p, "Please wait...", 0);
						}
					}
				}
				i--;
				if (this.i <= 0) {
					gameTimeOut();
				}
			}

		}, 0L, 20L);
		this.foodTick = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(dd, new Runnable() {
			public void run() {
				dd.$(0, "Giving players some food!");
				for (Player p : Bukkit.getServer().getWorld(world.getName()).getPlayers()) {
					p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF, 1));
				}
			}
		}, 0L, foodTime);
	}

	private void scoreboard() {
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();
		objective = board.registerNewObjective("height", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Deadly Descent");
		//Team VIPTeam = board.registerNewTeam("VIP");
		//Team AdminTeam = board.registerNewTeam("Admin");
		//Team ModeratorTeam = board.registerNewTeam("Moderator");
		//VIPTeam.setPrefix(ChatColor.GREEN+"");
		//AdminTeam.setPrefix(ChatColor.RED+"");
		//ModeratorTeam.setPrefix(ChatColor.GOLD+"");
		for(Player p : Bukkit.getServer().getOnlinePlayers()){
			if(p.getWorld().equals(world)){
				p.setScoreboard(board);
			}
		}
	}

	@EventHandler(priority=EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerMoveEvent(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (!game) {
			System.out.println("Game is off...");
			return;
		} else if  (player.getWorld().getName() != world.getName()) {
			//dd.$(0, "Wong map, got: " + player.getWorld().getName() + " while expecting " + world.getName());
			return;
		}

		Location loc = event.getTo();
		int yLoc = loc.getBlockY();
		String p = player.getDisplayName();
		if (p.length() > 16) {
			p = player.getName();
		}
		objective.getScore(p).setScore(yToWin - yLoc);
	}
	
	/*@EventHandler
	public void onEntityDamageEvent(EntityDamageEvent event) {
		
	}*/

	@EventHandler
	public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent e) {
		dd.$(0, "Picked up EntityDamageByEntityEvent!");
		if(e.getEntity() instanceof Player) {
			dd.$(0, "Picked up a player!");
			Player p = (Player) e.getEntity();
			if(p.getWorld().getName() == world.getName()) {
				dd.$(0, "Player is in world!");
				if((p.getHealth()-e.getDamage()) <= 0) {
					dd.$(0, "Cancelled event!");
					e.setCancelled(true);
					for (Player player : p.getWorld().getPlayers()) {
						dd.$(0, "Broadcasted message!");
						player.sendMessage(ChatColor.YELLOW + p.getName() + " was killed by " + e.getDamager().toString());
					}
					dd.$(0, "TPed to world spawn!");
					p.teleport(world.getSpawnLocation());
					p.setHealth(20);
				}
			}
		}
	}

	private void gameTimeOut() {
		dd.gameInProgress = false;
		Bukkit.getScheduler().cancelTask(this.gameTick);
		dd.$(0, "Cancelled gameTick.");
		Bukkit.getScheduler().cancelTask(this.foodTick);
		dd.$(0, "Cancelled foodTick.");
		PlayerMoveEvent.getHandlerList().unregister(this);
		dd.$(0, "Unregisteded PlayerMoveEvent.");
		EntityDamageByEntityEvent.getHandlerList().unregister(this);
		dd.$(0, "Unregisteded EntityDamageByEntityEvent.");
		dd.$(1, "Game ended with no winners!");
		for (Player p : Bukkit.getServer().getWorld(world.getName()).getPlayers()) {
			BarAPI.setMessage(p, "Game ended with no winners!");
			p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Deadly Descent" + ChatColor.RESET + ChatColor.RED + " � " + ChatColor.RESET +"Game ended with no winners, come on!");
		}
		this.lobbyTP = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(dd, new Runnable() {
			int i = lobbyTPTime;
			public void run() {
				objective.setDisplayName(ChatColor.AQUA + "Warping to lobby in " + ChatColor.GOLD + i);
				if (i <= 0) {
					warpToLobby();
					return;
				}
				i--;
			}
		}, 0L, 20L);
	}

	private void warpToLobby() {
		Bukkit.getScheduler().cancelTask(this.lobbyTP);
		game = false;
		for (Player p : Bukkit.getServer().getWorld(world.getName()).getPlayers()) {
			BarAPI.setMessage(p, "Game ended with no winners!");
			p.teleport(dd.getLobbyWorld().getSpawnLocation());
			p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		}
		return;
	}
}

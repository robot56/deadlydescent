package ml.extremenetwork.plugins.deadlydescent;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import me.confuser.barapi.BarAPI;

public class lobby implements Listener {
	DeadlyDescent dd = DeadlyDescent.plugin;
	public static String Prefix;
	private lobbyTimer lT;
	private Boolean timerStarted;

    public lobbyTimer countdown() {
    	return this.lT;
    }
	public Location getLobbySpawn(String worldName) {
		try {
			return Bukkit.getServer().getWorld(worldName).getSpawnLocation();
		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid world '" + worldName + "' specified, change it in the config!", e);
		}
	}
	
	@EventHandler
	public void onPlayerLoginEvent(PlayerLoginEvent event) {
		DeadlyDescent dd = DeadlyDescent.plugin;
    	if (dd.gameInProgress) {
    		event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Game is in progress, please try again later!");
    		return;
    	}
	}
	
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
    	DeadlyDescent dd = DeadlyDescent.plugin;
    	Player p = event.getPlayer();
    	event.setJoinMessage(getPlayerJoinMessage(p.getDisplayName()));
        p.sendMessage(ChatColor.DARK_GREEN + "" + ChatColor.STRIKETHROUGH + "��---------------�" + ChatColor.GOLD + ChatColor.BOLD + " Deadly Descent " + ChatColor.DARK_GREEN + ChatColor.STRIKETHROUGH + "�---------------��");
        p.sendMessage(" ");
        p.sendMessage(ChatColor.BOLD + "             Get to the bottom first without dieing!");
        p.sendMessage(" ");
        p.sendMessage(ChatColor.DARK_GREEN + "" + ChatColor.STRIKETHROUGH + "��---------------�----------------" + "�---------------��");
        p.teleport(dd.getLobbyLocation());
        p.getEquipment().clear();
    	p.getInventory().clear();
        if (!p.isOp()) {
        	p.setGameMode(GameMode.ADVENTURE);
        	p.setHealth(20.0);
        	p.setFoodLevel(20);
        }
        if (dd.getConfig().getBoolean("game.flight")) {
        	p.setFlying(true);
        }
        if (timerStarted == null || !timerStarted) {
        	int players = Bukkit.getServer().getWorld(dd.getLobbyName()).getPlayers().size() + 1; //+1 as the event is called before player join
        	dd.$(0, "Players: " + players);
        	dd.$(0, "[] of players: " + Bukkit.getServer().getWorld(dd.getLobbyName()).getPlayers().toString());
        	for(Player player : Bukkit.getOnlinePlayers()){
        		if(player.getWorld().getName().equals(dd.getLobbyName())){
        			BarAPI.setMessage(player, "Waiting for " + (dd.getMinPlayers() - players) + " more players!");
        		}
        	}
        	if (players == dd.getMinPlayers()) {
        		dd.$(0, "Starting timer...");
        		setTimerStarted(true);
        	} else return;
        }
        this.lT = new lobbyTimer(); //init countdown
        countdown().start(dd.getLobbyWaitTime());
    }
    
    @EventHandler
    public void onPlayerChangedWorldEvent(PlayerChangedWorldEvent event) {
    	DeadlyDescent dd = DeadlyDescent.plugin;
    	dd.$(0, "Picked up player world change!");
    	Player p = event.getPlayer();
    	if (p.getWorld().equals(dd.getLobbyWorld())) {
            p.getEquipment().clear();
        	p.getInventory().clear();
            if (!p.isOp()) {
            	p.setGameMode(GameMode.ADVENTURE);
            	p.setHealth(20.0);
            	p.setFoodLevel(20);
            }
            if (dd.getConfig().getBoolean("game.flight")) {
            	p.setFlying(true);
            }
            if (timerStarted == null || !timerStarted) {
            	int players = Bukkit.getServer().getWorld(dd.getLobbyName()).getPlayers().size();
            	dd.$(0, "Players: " + players);
            	dd.$(0, "[] of players: " + Bukkit.getServer().getWorld(dd.getLobbyName()).getPlayers().toString());
            	for(Player player : Bukkit.getOnlinePlayers()){
            		if(player.getWorld().getName().equals(dd.getLobbyName())){
            			BarAPI.setMessage(player, "Waiting for " + (dd.getMinPlayers() - players) + " more players!");
            		}
            	}
            	if (players == dd.getMinPlayers()) {
            		dd.$(0, "Starting timer...");
            		setTimerStarted(true);
            	} else return;
            }
            this.lT = new lobbyTimer(); //init countdown
            countdown().start(dd.getLobbyWaitTime());
    	}
    }
    
    private void setTimerStarted(Boolean started) {
    	if (started) {
    		timerStarted = true;
    	} else {
    		timerStarted = false;
    	}
    }
    
    private String getPlayerJoinMessage(String player) {
    	DeadlyDescent dd = DeadlyDescent.plugin;
    	int lobbyPlayers = Bukkit.getServer().getWorld(dd.getLobbyName()).getPlayers().size() + 1;
    	if (lobbyPlayers > dd.getMaxPlayers()) {
    		Bukkit.getServer().getPlayer(player).kickPlayer(ChatColor.GOLD + "Deadly Descent: " + ChatColor.RED + "The game is full!");
    	}
    	String message = ChatColor.AQUA + "[" + lobbyPlayers + "/" + dd.getMaxPlayers() + "] " + ChatColor.GREEN + ChatColor.BOLD + player + " has joined!";
    	return message;
    }
    
    @EventHandler
    public void onPlayerKickEvent(PlayerKickEvent event) {
    	event.setLeaveMessage("");
    }
    
    @EventHandler
    public void onEnitityDamage(EntityDamageEvent event) {
    	DeadlyDescent dd = DeadlyDescent.plugin;
        Entity e = event.getEntity();
        //if (e instanceof Player) {
        //	Player p = (Player) e;
        if (e.getWorld().getName().equalsIgnoreCase(dd.getLobbyName())) {
        	event.setCancelled(true);
        }
        //}
    }
    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event)
    {
    	DeadlyDescent dd = DeadlyDescent.plugin;
    	if (event.getEntity().getWorld().getName().equalsIgnoreCase(dd.getLobbyName())) {
    		event.setCancelled(true);
    	}
    }
    
    @EventHandler
    public void onBlockDamageEvent(BlockDamageEvent event) {
    	DeadlyDescent dd = DeadlyDescent.plugin;
    	if (!dd.isLobbyProtected()) return;
    	if (event.getPlayer().getWorld().getName().equalsIgnoreCase(dd.getLobbyName())) {
    		event.setCancelled(true);
    	}
    }
}
